<?php
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "l5";
	
	//$conn = mysql_connect($servername, $username, $password);
	$conn = mysqli_connect($servername, $username, $password  , $dbname);
	
	$sql = "SELECT * FROM customer";
	$result = $conn->query($sql);
	$conn->close();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="style.css">
		<link rel="stylesheet" href="bootstrap.min.css">
    </head>
    <body>
	
	<div class="col-md-offset-2 col-md-8 middle">
	<div class="form">
	<form action="add.php" method="post" enctype="multipart/form-data">
		<table  class="table">
		<tr>
			<th>Name</th>
			<th>Surname</th>
			<th>Age</th>
		</tr>
		<tr>
			<td><input type="text" name="name"/></td>
			<td><input type="text" name="surname"/></td>
			<td><input type="number" name="age"/></td>
		</tr>
		</table>
		<button type="submit" class="btn btn-default left">Add</button>
	</form>
	</div>
	<table  class="table">
		<tr>
			<th>Name</th>
			<th>Surname</th>
			<th>Age</th>
			<th></th>
		</tr>
		<?php
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo "<tr>";
					echo "<td>" . $row['name'] . "</td>";
					echo "<td>" . $row['surname'] . "</td>";
					echo "<td>" . $row['age'] . "</td>";
					echo "<td>" 
					.'<a href="/delete.php?id='.$row['id'].'">Delete</a>'." | "
					.'<a href="/edit.php?id='.$row['id'].'">Edit</a>'
					."</td>";
				echo "</tr>";
			}	
		}
		?>
	</table>
	</div>

    </body>
</html>