<?php
$lang = array(
    "title" => "Лабораторна робота 11",
    "kyiv" => "Київ",
    "zhytomyr" => "Житомир",
    "lviv" => "Львів",
    "football" => "Футбол",
    "basketball" => "Баскетбол",
    "volleyball" => "Волейбол",
    "chess" => "Шахи",
    "WOT" => "World Of Tanks",
    "login" => "Логін",
    "password" => "Пароль",
    "password_2" => "Підтвердження паролю",
    "gender" => "Стать",
    "male" => "Чоловіча",
    "female" => "Жіноча",
    "fav_games" => "Улюблені ігри",
    "about" => "Про себе",
    "photo" => "Фото",
    "sign_in" => "Зареєструватися",
    "selected_lang" => "Вибрана мова"
);