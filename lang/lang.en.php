<?php
$lang = array(
    "title" => "LR 11",
    "kyiv" => "Kyiv",
    "zhytomyr" => "Zhytomyr",
    "lviv" => "Lviv",
    "football" => "Football",
    "basketball" => "Basketball",
    "volleyball" => "Volleyball",
    "chess" => "Chess",
    "WOT" => "World Of Tanks",
    "login" => "Login",
    "password" => "Password",
    "password_2" => "Confirm Password",
    "gender" => "Gender",
    "male" => "Male",
    "female" => "Female",
    "fav_games" => "Favorite games",
    "about" => "About you",
    "photo" => "Photo",
    "sign_in" => "Sign In",
    "selected_lang" => "Selected Language"
);