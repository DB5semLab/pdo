<?php
$lang = array(
    "title" => "Лабораторная работа 11",
    "kyiv" => "Киев",
    "zhytomyr" => "Житомир",
    "lviv" => "Львов",
    "football" => "Футбол",
    "basketball" => "Баскетбол",
    "volleyball" => "Волейбол",
    "chess" => "Шахматы",
    "WOT" => "World Of Tanks",
    "login" => "Логин",
    "password" => "Пароль",
    "password_2" => "Подтверждение пароля",
    "gender" => "Пол",
    "male" => "Мужской",
    "female" => "Женский",
    "fav_games" => "Любимые игры",
    "about" => "О себе",
    "photo" => "Фото",
    "sign_in" => "Зарегистрироваться",
    "selected_lang" => "Выбраный язык"
);